
Timing build of CMake
=====================

::

   curl -L https://github.com/Kitware/CMake/releases/download/v3.25.2/cmake-3.25.2.tar.gz -O
   tar xf cmake-3.25.2.tar.gz
   cmake -B build-cmake -GNinja -DCMAKE_BUILD_TYPE=Release cmake-3.25.2

   # accept Ninja defsult -j NNN:
   time cmake --build build-cmake

   # explicit -j NNN:
   cmake --build build-cmake --target clean
   time cmake --build build-cmake -j14

 
