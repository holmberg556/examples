

#include <iostream>
#include <future>
#include <chrono>

bool is_prime(uint32_t n)
{
  if (n % 2 == 0) return false;
  uint32_t d = 3;
  while (d * d <= n) {
    if (n % d == 0) return false;
    d += 2;
  }
  return true;
}

uint32_t nth_prime(uint32_t nth)
{
  if (nth == 1) return 2;
  for (uint32_t nfound=1, n=3; ; n += 2) {
    if (is_prime(n)) {
      nfound += 1;
      if (nfound == nth) return n;
    }
  }
}

void run_threads(uint32_t nth, int nthreads)
{
  auto t1 = std::chrono::high_resolution_clock::now();
  
  auto futs = new std::future<uint32_t>[nthreads];
  for (int i=0; i<nthreads; i++) {
    futs[i] = std::async(std::launch::async, nth_prime, nth);
  }

  uint32_t n = futs[0].get();
  for (int i=1; i<nthreads; i++) {
    if (futs[i].get() != n) {
      std::cout << "ERROR:\n";
      exit(1);
    }
  }

  auto t2 = std::chrono::high_resolution_clock::now();
  auto tdiff = (t2-t1) / std::chrono::milliseconds(1);
  
  std::cout << "nth prime: " << n << "    [ " << nthreads << " : " << tdiff << " ]\n";
  delete[] futs;
}


int main(int argc, char* argv[])
{
  uint32_t nth = std::stoi(argv[1]);
  int nt1 = std::stoi(argv[2]);
  int nt2 = std::stoi(argv[3]);

  for (int nthreads=nt1; nthreads <= nt2; nthreads++) {
    run_threads(nth, nthreads);
  }
  return 0;
}
