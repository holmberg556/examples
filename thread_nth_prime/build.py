#!/usr/bin/env python3

from subprocess import run

import shutil
import os
import sys

WINDOWS = (sys.platform == 'win32')

def sh(cmd):
    print('>>>', cmd)
    run(cmd, shell=True, check=True)

if os.path.exists('build'):
    shutil.rmtree('build')

if WINDOWS:
    sh('cmake -B build src')
    sh('cmake --build build --config Release')
else:
    sh('cmake -DCMAKE_BUILD_TYPE=Release -B build src')
    sh('cmake --build build')



