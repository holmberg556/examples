
ORIG_CWD = Dir.pwd

PARALLEL_EXEC = ENV["PARALLEL_EXEC"] || "parallel_exec_cpp"

class CMDTEST_parallel_exec < Cmdtest::Testcase

    def setup
    end

    def teardown
    end

    def _next_second
        t1 = Time.now.to_i
        loop do
            t2 = Time.now.to_i
            return t2 if t2 > t1
        end
    end

    def test_1
        ENV['CURRENT_TIME'] = _next_second.to_s
        cmd "#{PARALLEL_EXEC} 11 < #{ORIG_CWD}/cmd_seq.sh" do
            stdout_equal [
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
            ]
        end
        ENV['CURRENT_TIME'] = _next_second.to_s
        cmd "#{PARALLEL_EXEC} 10 < #{ORIG_CWD}/cmd_seq.sh" do
            stdout_equal [
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 2',
            ]
        end
        ENV['CURRENT_TIME'] = _next_second.to_s
        cmd "#{PARALLEL_EXEC} 6 < #{ORIG_CWD}/cmd_seq.sh" do
            stdout_equal [
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 2',
            ]
        end
        ENV['CURRENT_TIME'] = _next_second.to_s
        cmd "#{PARALLEL_EXEC} 5 < #{ORIG_CWD}/cmd_seq.sh" do
            stdout_equal [
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 3',
            ]
        end
        ENV['CURRENT_TIME'] = _next_second.to_s
        cmd "#{PARALLEL_EXEC} 4 < #{ORIG_CWD}/cmd_seq.sh" do
            stdout_equal [
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 3',
                'slow_echo: 3',
                'slow_echo: 3',
            ]
        end
        ENV['CURRENT_TIME'] = _next_second.to_s
        cmd "#{PARALLEL_EXEC} 3 < #{ORIG_CWD}/cmd_seq.sh" do
            stdout_equal [
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 3',
                'slow_echo: 3',
                'slow_echo: 3',
                'slow_echo: 4',
                'slow_echo: 4',
            ]
        end
        ENV['CURRENT_TIME'] = _next_second.to_s
        cmd "#{PARALLEL_EXEC} 2 < #{ORIG_CWD}/cmd_seq.sh" do
            stdout_equal [
                'slow_echo: 1',
                'slow_echo: 1',
                'slow_echo: 2',
                'slow_echo: 2',
                'slow_echo: 3',
                'slow_echo: 3',
                'slow_echo: 4',
                'slow_echo: 4',
                'slow_echo: 5',
                'slow_echo: 5',
                'slow_echo: 6',
            ]
        end
        ENV['CURRENT_TIME'] = _next_second.to_s
        cmd "#{PARALLEL_EXEC} 1 < #{ORIG_CWD}/cmd_seq.sh" do
            stdout_equal [
                'slow_echo: 1',
                'slow_echo: 2',
                'slow_echo: 3',
                'slow_echo: 4',
                'slow_echo: 5',
                'slow_echo: 6',
                'slow_echo: 7',
                'slow_echo: 8',
                'slow_echo: 9',
                'slow_echo: 10',
                'slow_echo: 11',
            ]
        end
    end

end
