#!/usr/bin/perl
use strict;
my $tbase = $ENV{CURRENT_TIME} // time();
my $t1 = time();
my $t2 = $t1;
while ($t2 == $t1) {
    $t2 = time();
}
print "slow_echo: ".($t2-$tbase)."\n";
