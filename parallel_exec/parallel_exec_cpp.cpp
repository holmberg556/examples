

#include <iostream>
#include <string>
#include <vector>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

using namespace std;

typedef vector<string> string_arr_t ;


int process_start(string & command) {
    int pid = vfork();
    if (pid > 0) {
        // in parent
        return pid;
    }
    else if (pid == 0) {
        // in child
        execlp("sh", "sh", "-c", command.c_str(), nullptr);
    }
    _exit(126);
}

void process_wait(int & res_pid, int & res_status) {
    int stat_loc;
    // TODO: compare usage with AUP
    int pid = waitpid(0, &stat_loc, 0);
    if (pid == -1) {
        perror("waitpid");
        exit(1);
    }
    res_pid = pid;
    res_status = stat_loc;
}

int
main(int argc, char* argv[])
{
    int maxrunning = 1;
    if (argc > 1) {
        maxrunning = atoi(argv[1]);
    }
    bool verbose = (argc > 2);

    string command;
    string_arr_t commands;
    while (getline(cin, command)) {
        commands.push_back(command);
    }

    if (verbose) {
        cout << "#commands = " << commands.size() << "\n";
        cout << flush;
    }

    int next = 0;
    int nrunning = 0;
    while (next < commands.size() || nrunning > 0) {
        while (next < commands.size() && nrunning < maxrunning) {
            process_start(commands[next]);
            next++;
            nrunning++;
        }
        int pid;
        int exitstatus;
        process_wait(pid, exitstatus);
        nrunning--;
        if (exitstatus != 0) {
            cout << "ERROR: ...\n";
            exit(1);
        }
    }

    return 0;
}
