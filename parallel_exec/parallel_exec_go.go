package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
)

func process_run(command string) {
	cmd := exec.Command("sh", "-c", command)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	err = cmd.Wait()
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	maxrunning := 1
	if len(os.Args) > 1 {
		maxrunning, _ = strconv.Atoi(os.Args[1])
	}
	verbose := (len(os.Args) > 2)

	scanner := bufio.NewScanner(os.Stdin)
	var commands []string
	for scanner.Scan() {
		commands = append(commands, scanner.Text())
	}

	if verbose {
		fmt.Println("#commands =", len(commands))
	}

	todo := make(chan string)
	done := make(chan struct{})

	go func() {
		for _, command := range commands {
			todo <- command
		}
		close(todo)
	}()

	for i := 0; i < maxrunning; i++ {
		go func() {
			for command := range todo {
				process_run(command)
			}
			done <- struct{}{}
		}()
	}

	for i := 0; i < maxrunning; i++ {
		<- done
	}
}
