#!/usr/bin/perl

sub process_start {
    my ($command) = @_;

    my $pid = fork();
    if ($pid > 0) {
        # in parent
        return $pid;
    }
    elsif ($pid == 0) {
        ## in child
        exec($command);
    }
    exit(126);
}

sub process_wait {
    my ($r_res_pid, $r_res_status) = @_;
    my $pid = wait();
    if ($pid == -1) {
        die("wait");
    }
    $$r_res_pid = $pid;
    $$r_res_status = $?;
}

sub main {
    $| = 1;
    my $maxrunning = $ARGV[0] || 1;
    my $verbose = @ARGV > 1;

    my @commands = <STDIN>;
    chomp @commands;

    if ($verbose) {
        print "#commands = ", scalar(@commands), "\n";
    }

    my $next = 0;
    my $nrunning = 0;
    while ($next < @commands || $nrunning > 0) {
        while ($next < @commands && $nrunning < $maxrunning) {
            process_start($commands[$next]);
            $next++;
            $nrunning++;
        }
        my $pid;
        my $exitstatus;
        process_wait(\$pid, \$exitstatus);
        $nrunning--;
        if ($exitstatus != 0) {
            die("ERROR: ...");
        }
    }

}

main();
