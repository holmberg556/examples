
import std.conv;
import std.stdio;
import std.string;
import std.process;
import core.stdc.stdlib;
import core.sys.posix.unistd;
import core.sys.posix.sys.wait;

int process_start(string cmdline) {
    int pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(1);
    }
    else if (pid == 0) {
      string[] argv = ["sh", "-c", cmdline];
      execvp("sh", argv);
      _exit(126);
    }
    else {
        return pid;
    }
    assert(0);
}

void process_wait(out int pid, out int status) {
    pid = waitpid(0, &status, 0);
    if (pid == -1) {
        perror("waitpid");
        exit(1);
    }
}

int
main(string[] argv)
{
    int maxrunning = 1;
    if (argv.length > 1) {
        maxrunning = to!int(argv[1]);
    }
    bool verbose = (argv.length > 2);

    string command;
    string[] commands;
    foreach (line; stdin.byLine()) {
        commands ~= line.idup;
    }

    if (verbose) {
        writeln("#parallel = ", maxrunning);
        writeln("#commands = ", commands.length);
        stdout.flush();
    }

    int next = 0;
    int nrunning = 0;
    while (next < commands.length || nrunning > 0) {
        while (next < commands.length && nrunning < maxrunning) {
            process_start(commands[next]);
            next++;
            nrunning++;
        }
        int pid;
        int exitstatus;
        process_wait(pid, exitstatus);
        nrunning--;
        if (exitstatus != 0) {
            writeln("ERROR: ...");
            exit(1);
        }
    }
    return 0;
}
