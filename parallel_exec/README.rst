
parallel_exec
=============

This is an attempt to implement the basic functionality of ``parallel -u -jN``
(ie. GNU Parallel) in different languages to compare the speed that can be
attained in each language.

Each program essentially does:

- reads a list of commands from STDIN

- stores the list of commands in the program

- loops over the list and executes each command, running N commands at the same time


The first set of test data used was a list of 85000 C/C++ compilation
commands, with each command being around 700 characters long (several
long include paths). The maximum value of N used was 24 when running
on a machine with that many cores.


Building the programs
---------------------

This can be done by::

  rake make

see the ``Rakefile`` file for details.


Testing the programs
--------------------

For testing the basic functionality of each program, Cmdtest has been used.
It is run like this::

  $ cmdtest
  $ env PARALLEL_EXEC=parallel_exec_cpp cmdtest        # same as first
  $ env PARALLEL_EXEC=parallel_exec_dlang cmdtest
  $ env PARALLEL_EXEC=parallel_exec_dlang_spawn cmdtest
  $ env PARALLEL_EXEC=parallel_exec_go cmdtest

The tests are in the file ``CMDTEST_parallel_exec.rb``. This is the input file to ``cmdtest``.


Benchmarking
------------

...to be written...
