
#pragma once

#ifdef _WIN32
#  ifndef BBB_EXPORT
#    ifdef bbb_EXPORTS
#      define BBB_EXPORT __declspec(dllexport)
#    else
#      define BBB_EXPORT __declspec(dllimport)
#    endif
#  endif
#elif __GNUC__
#  define BBB_EXPORT __attribute__ ((visibility("default")))
#else
#  define BBB_EXPORT
#endif

void BBB_EXPORT BbbFun(void);
