
#include "aaa.h"
#include "mylib.h"

#include <iostream>

int aaa_i = 123;

//----------------------------------------------------------------------

void AaaFun(void)
{
  std::cout << "aaa: ...\n";
  ShowResources("aaa");
}
