
#include "mylib.h"

#include <iostream>
#include <stdlib.h>

#include <afx.h>

//----------------------------------------------------------------------

HMODULE GetHandle(bool appl)
{
  if (appl) {
    return GetModuleHandle(nullptr);
  }
  
  HMODULE hModule = NULL;
  bool ok = GetModuleHandleEx((GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
                               GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT),
                              (LPCTSTR) &GetHandle,
                              &hModule);
  if (!ok) {
    std::cout << "ERROR: GetModuleHandleEx\n";
    exit(1);
  }
  return hModule;
}

//----------------------------------------------------------------------

std::string MyLoadString(int resid, bool appl)
{
  char buffer[1000];
  buffer[0] = '\0';

  CString string;
  string.LoadString(resid);
  std::string extra = std::string(" --- ") + string.GetString();

  int n = LoadString(GetHandle(appl),
                     resid,
                     buffer, 999);
  if (n == 0) {
    return "<<<not defined>>>" + extra;
  }
  else {
    return std::string(buffer) + extra;
  }
}

//----------------------------------------------------------------------

void ShowResources(std::string prefix)
{
  std::cout << "======================================== " << prefix << "\n";
  for (int i=10000; i<10005; i++) {
    std::cout << "LoadString(" << i << ") = " << MyLoadString(i, false) << "\n";
  }
  std::cout << "\n";
  for (int i=10000; i<10005; i++) {
    std::cout << "appl: LoadString(" << i << ") = " << MyLoadString(i, true) << "\n";
  }
  std::cout << "\n";
}

//----------------------------------------------------------------------
