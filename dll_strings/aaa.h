
#pragma once

#ifdef _WIN32
#  ifndef AAA_EXPORT
#    ifdef aaa_EXPORTS
#      define AAA_EXPORT __declspec(dllexport)
#    else
#      define AAA_EXPORT __declspec(dllimport)
#    endif
#  endif
#elif __GNUC__
#  define AAA_EXPORT __attribute__ ((visibility("default")))
#else
#  define AAA_EXPORT
#endif

void AAA_EXPORT AaaFun(void);
