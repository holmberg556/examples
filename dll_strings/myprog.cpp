
#include "aaa.h"
#include "bbb.h"
#include "mylib.h"

#include <iostream>

#include "afx.h"

int main()
{
  std::cout << "main: ...\n";
  ShowResources("main");
  
  AaaFun();
  BbbFun();

  return 0;
}
